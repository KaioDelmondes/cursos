module.exports.cadastro = (application, req, res) => {
    res.render('cadastro', {validacao: {}, dadosForm:{}});
}

module.exports.cadastrar = (application, req, res) => {
    var dbConnection = application.config.dbConnection;
    var UsuariosDAO = new application.app.models.UsuariosDAO(dbConnection);
    var JogoDAO = new application.app.models.JogoDAO(dbConnection);
    var dadosForm = req.body;
    
    req.assert('nome', 'Nome não pode ser vazio').notEmpty();
    req.assert('usuario', 'Usuário não pode ser vazio').notEmpty();
    req.assert('senha', 'Senha não pode ser vazio').notEmpty();
    req.assert('casa', 'Casa não pode ser vazio').notEmpty();
    
    var errors = req.validationErrors();
    
    if(errors){
        res.render('cadastro', {validacao: errors, dadosForm: dadosForm});
        return;
    }
    
    UsuariosDAO.inserirUsuario(dadosForm);
    JogoDAO.gerarValores(dadosForm.usuario);

    res.redirect('/jogo');
}