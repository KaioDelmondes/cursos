const params = require('dotenv').config().parsed;
const { Pool } = require('pg');

var pgConnector = function(){
    return new Pool(params);
}

module.exports = function(){
    return pgConnector;
};