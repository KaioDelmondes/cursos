function NoticiasDAO(dbConnection){
    this._dbConnection = dbConnection;
};

NoticiasDAO.prototype.getNoticias = function(callback){
    this._dbConnection.query('SELECT * FROM noticias ORDER BY data_criacao DESC', callback);
};

NoticiasDAO.prototype.getNoticia = function(id_noticia, callback){
    this._dbConnection.query('SELECT * FROM noticias WHERE id_noticia = $1', id_noticia, callback);
};

NoticiasDAO.prototype.salvarNoticia = function(content, callback){
    this._dbConnection.query('INSERT INTO noticias(titulo,resumo,autor,data_noticia,noticia)VALUES($1,$2,$3,$4,$5)', content, callback);
};

NoticiasDAO.prototype.get5UltimasNoticias = function(callback){
    this._dbConnection.query(
        'SELECT * FROM noticias ORDER BY data_criacao DESC LIMIT 5',
        callback
    );
};

module.exports = function(){
    return NoticiasDAO;
}