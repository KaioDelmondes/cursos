module.exports = (application) => {
    application.get('/jogo', (req, res) => {
        application.app.controllers.jogo.iniciarJogo(application, req, res);
    });

    application.get('/sair', (req, res) => {
        application.app.controllers.jogo.sair(application, req, res);
    });

    application.get('/pergaminhos', (req, res) => {
        application.app.controllers.jogo.pergaminhos(application, req, res);
    });

    application.get('/aldeoes', (req, res) => {
        application.app.controllers.jogo.aldeoes(application, req, res);
    });

    application.post('/ordenar_acao_sudito', (req, res) => {
        application.app.controllers.jogo.ordenar_acao_sudito(application, req, res);
    });

    application.get('/revogar_ordem', (req, res) => {
        application.app.controllers.jogo.revogarOrdem(application, req, res);
    });
}