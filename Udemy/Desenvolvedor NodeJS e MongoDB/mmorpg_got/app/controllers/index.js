module.exports.home = (application, req, res) => {
    res.render('index', {validacao: {}, dadosForm: {}});
}

module.exports.autenticar = (application, req, res) => {
    var dadosForm = req.body;
    
    req.assert('usuario', 'Usuário não pode ser vazio').notEmpty();
    req.assert('senha', 'Senha não pode ser vazio').notEmpty();
    
    var errors = req.validationErrors();
    
    if(errors){
        res.render('index', {validacao: errors, dadosForm: dadosForm});
        return;
    }

    const dbConnection = application.config.dbConnection;
    const UsuariosDAO = new application.app.models.UsuariosDAO(dbConnection);

    UsuariosDAO.autenticar(dadosForm, req, res);
}