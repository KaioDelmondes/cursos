module.exports.index = function(application, req, res){
    const dbConnection = application.config.dbConnection();
    const NoticiasDAO = new application.app.models.NoticiasDAO(dbConnection);

    NoticiasDAO.get5UltimasNoticias(function(err, result){
        if(err){
            console.log(err);
        }
        else{
            res.render('home/index', {noticias:result.rows});
        }
    });
}