var app = require('./config/server');

var server = app.listen(80, () => {
    console.log('Servidor aberto na porta 80');
});

var io = require('socket.io').listen(server);
app.set('io', io);

io.on('connection', function(socket){
    console.log('Usuário se Conectou');

    socket.on('msgParaServidor', function(data){
        socket.emit(
            'msgParaCliente',
            {apelido : data.apelido, mensagem : data.mensagem}
        );

        socket.broadcast.emit(
            'msgParaCliente',
            {apelido : data.apelido, mensagem : data.mensagem}
        );
        
        if(parseInt(data.clienteJaAdicionado) == 0){
            socket.emit(
                'participantesParaCliente',
                {participante : data.apelido}
            );
    
            socket.broadcast.emit(
                'participantesParaCliente',
                {participante : data.apelido}
            );
        }
    });

    socket.on('disconnect', function(){
        console.log('Usuário se Desconectou')
    })
});