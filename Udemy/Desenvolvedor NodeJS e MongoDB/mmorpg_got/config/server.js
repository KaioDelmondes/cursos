var express = require('express');
var consign = require('consign');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');
var expressSession = require('express-session');

app = express();

app.set('views', './app/views');
app.set('view engine', 'ejs');

app.use(expressSession({
    secret: 'kaiodelmondes',
    resave: false,
    saveUninitialized: false
}));

app.use(expressValidator());

app.use(bodyParser.urlencoded({
    extended:true
}));

app.use(express.static('./app/public'));

consign()
    .include('./app/routes')
    .then('./config/dbConnection.js')
    .then('./app/controllers')
    .then('./app/models')
    .into(app);

module.exports = app;