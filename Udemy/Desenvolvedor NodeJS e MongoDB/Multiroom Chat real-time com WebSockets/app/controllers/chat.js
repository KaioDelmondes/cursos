module.exports.iniciaChat = (application, req, res) => {
    req.assert('apelido', 'O apelido não pode ser vazio').notEmpty();
    req.assert('apelido', 'O apelido deve ter entre 3 e 10 letras').len(3,10);

    var errors = req.validationErrors();
    if(errors){
        res.render('index', {validacao:errors});
        return;
    }

    const dadosForm = req.body;
    application.get('io').emit(
        'msgParaCliente',
        {apelido : dadosForm.apelido, mensagem : ' acabou de entrar no Chat.'}
    );


    res.render('chat', {dadosForm : dadosForm});
}