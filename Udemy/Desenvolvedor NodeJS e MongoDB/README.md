## Curso Completo do Desenvolvedor NodeJS e MongoDB

#### Conteúdo
Este curso cobre conceitos básicos de desenvolvimento web, tais como: autenticação, negociação de conteúdo, criptografia de dados sensíveis do usuário e validação de campos de formulário. Junto ao Node.JS, utiliza os pacotes Express, para construção de servidores web, consign para autoload de módulos dentro da aplicação web, crypto na criptografia de dados do usuário.

O mongoDB foi usado em 2 projetos nessa aplicação, assim foi introduzido explicando sua diferença em relação aos bancos relacionais tradicionais e com comandos básicos: find, remove, update, save e o uso de comandos básicos $inc, $set, $pull, $push além de operadores lógicos.

Este curso utilizou também o SGBD MySQL na construção de aplicações, mas esta implementação decidiu pelo uso do PostgreSQL que também é bastante utilizado.

#### Projetos Práticos
 - Portal de Notícias: Uma aplicação que envolve um CRUD de notícias.
 - Multiroom Chat real-time com WebSockets: Autodescritivo.
 - MMORPG GoT: Uma aplicação que simula um jogo rpg com temática de GoT. Autenticação de usuário e criptografia de dados foi usada, validação de formulário e requisições AJAX.
 - API RestFul e Instagram Web Clone: Duas aplicações, um servidor HTTP Rest, e um frontweb simples. Autenticação, upload e manipulação de arquivos no servidor.

#### Pacotes
 - Express: Implementação de servidores web.
 - EJS: Processador de templates.
 - Nodemon: Atualização de arquivos modificados no servidor. * Não é uma dependência do projeto.
 - Consign: Autoload que carrega os módulos na aplicação para organizar a estrutura do projeto.
 - Body-parser: Processa os dados enviados pela request e os deixa disponível no objeto req.
 - Express-validator: Middleware para validação de formulário, usado junto com o body-parser.
 - Express-static: Middleware para mapeação de diretório de arquivos estáticos na aplicação. Útil para templates, imagens e scripts.
 - Express-session: Criação de sessão para o usuário, salvando informações que são constantemente usadas.
 - Crypto: Pacote com funções de hash para criptografar os dados.
 - XMLHttpRequest: Não é um pacote ou biblioteca instalado, mas é usado no front para fazer requisições via script.
 - Node-postgre: Driver do PostgreSQL para JavaScript.
 - mongodb: Drivero do MongoDB para JavaScript.