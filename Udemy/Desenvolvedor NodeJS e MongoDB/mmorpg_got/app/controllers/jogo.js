module.exports.iniciarJogo = (application, req, res) => {
    if(req.session.authorized !== true)
    {
        res.send('O Usuário não está autenticado');
        return;
    }
    else
    {
        var msg = '';
        if(req.query.msg != ''){
            msg = req.query.msg;
        }
        const dbConnection = application.config.dbConnection;
        const JogoDAO = new application.app.models.JogoDAO(dbConnection);
        JogoDAO.iniciarJogo(res, req.session.usuario, req.session.casa, msg);
    }
}
module.exports.sair = (application, req, res) => {
    req.session.destroy(function(err, result){
        if(err)
        {
            res.send(err);
            throw err;
        }
        else
        {
            res.render('index', {validacao:{}, dadosForm:{}});
        }
    });
}
module.exports.pergaminhos = (application, req, res) => {
    if(req.session.authorized !== true)
    {
        res.send('O Usuário não está autenticado');
        return;
    }
    const dbConnection = application.config.dbConnection;
    const JogoDAO = new application.app.models.JogoDAO(dbConnection);
    
    JogoDAO.getAcoes(req.session.usuario, res);
}
module.exports.aldeoes = (application, req, res) => {
    if(req.session.authorized !== true)
    {
        res.send('O Usuário não está autenticado');
        return;
    }

    res.render('aldeoes', {validacao:{}});
}
module.exports.ordenar_acao_sudito = (application, req, res) => {
    if(req.session.authorized !== true)
    {
        res.send('O Usuário não está autenticado');
        return;
    }
    req.assert('acao').notEmpty();
    req.assert('quantidade').notEmpty()
    const errors = req.validationErrors();
    if(errors){
        res.redirect('/jogo?msg=A');
        return;
    }
    
    const acaoInfo = req.body;
    acaoInfo.usuario = req.session.usuario;

    const dbConnection = application.config.dbConnection;
    const JogoDAO = new application.app.models.JogoDAO(dbConnection);

    var tempo = null;
    var moedas = null;
    switch(parseInt(acaoInfo.acao)){
        case 1: 
            moedas = -2 * parseInt(acaoInfo.quantidade);
            tempo = 1 * 60 * 60000;
            break;
        case 2:
            moedas = -3 * parseInt(acaoInfo.quantidade);
            tempo = 2 * 60 * 60000;
            break;
        case 3:
            moedas = -1 * parseInt(acaoInfo.quantidade);
            tempo = 5 * 60 * 60000;
            break;
        case 4:
            moedas = -1 * parseInt(acaoInfo.quantidade);
            tempo = 5 * 60 * 60000;
            break;
    }
    
    acaoInfo.acao_termina_em = Date.now() + tempo;
    acaoInfo.moedas = moedas;
    
    console.log(acaoInfo);
    
    JogoDAO.novaAcao(acaoInfo);
    JogoDAO.setMoedas(acaoInfo, res);
}
module.exports.revogarOrdem = (application, req, res) => {
    const ordemId = req.query.id_ordem;
    const dbConnection = application.config.dbConnection;
    const JogoDAO = new application.app.models.JogoDAO(dbConnection);
    JogoDAO.revogarOrdem(ordemId, res);
}