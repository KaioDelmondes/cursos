var {MongoClient, Server, Db} = require('mongodb');
var params = require('dotenv').config();

if(params.error){
    throw error;
}

var mongoConnectorMongoClient = function(){

    var client = new MongoClient('mongodb://localhost:27017', { useUnifiedTopology: true });
    client.connect(function(err){
        if(err){
            console.log('-----ERRO DE CONEXÃO COM O MONGO: ' + err);
        }
        console.log('+++++++CONEXÃO ESTABELECIDA COM MONGO');
    });

    console.log('++++++++++NOME DO BD: ' + params.parsed.DBNAME);
    const db = client.db(params.parsed.DBNAME);
    return db;
}

module.exports = function(){
    return mongoConnectorMongoClient;
}

/* 
var mongoConnectorDb = function(){
    var sv = new Server('localhost', 27017,  { useUnifiedTopology: true } );
    var db = new Db('got', sv);
    db.stats(function(err, result){
        if(err){
            console.log('-----ERRO DE CONEXÃO COM O MONGO: ' + err);
        }
        console.log('+++++++CONEXÃO ESTABELECIDA COM MONGO: '+ result);
    });
    return db;
}
 */