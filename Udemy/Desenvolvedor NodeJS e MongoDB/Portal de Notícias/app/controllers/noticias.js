module.exports.noticia = function (application, req, res) {
    const dbConnection = application.config.dbConnection();
    const noticiaModel = new application.app.models.NoticiasDAO(dbConnection);

    const id_noticia = Object.values(req.query);

    noticiaModel.getNoticia(id_noticia, (err, result) => {
        if (err) {
            console.log(err);
            res.send(err);
        } else {
            res.render('noticias/noticia', { noticia: result.rows[0] });
        }
    });
}

module.exports.noticias = function (application, req, res) {
    const dbConnection = application.config.dbConnection();
    const noticiaModel = new application.app.models.NoticiasDAO(dbConnection);

    noticiaModel.getNoticias((err, result) => {
        if (err) {
            res.send(err);
            console.log(err);
        }
        else {
            res.render('noticias/noticias', { noticias: result.rows });
        }
    });
}