const crypto = require('crypto');

function UsuariosDAO(dbConnection){
    this._dbConnection = dbConnection();
}

UsuariosDAO.prototype.inserirUsuario = function(usuario){
    const passwordCypto = crypto.createHash('md5').update(usuario.senha).digest('hex');
    usuario.senha = passwordCypto;
    
    this._dbConnection.collection('usuarios')
        .insertOne(
            usuario,
            (err, result) => {
                if(err){
                    throw err;
                }
            }
        );
}

UsuariosDAO.prototype.autenticar = function(usuario, req, res){
    const passwordCypto = crypto.createHash('md5').update(usuario.senha).digest('hex');
    usuario.senha = passwordCypto;

    this._dbConnection.collection('usuarios')
        .find(usuario).toArray(function(err, result){
            if(err){
                console.log('-------ERRO NA PESQUISA: ' + err);
            }
            
            if(result[0] != undefined){
                req.session.authorized = true;
                
                req.session.usuario = result[0].usuario;
                req.session.casa = result[0].casa;
            }
            if(req.session.authorized){
                res.redirect('/jogo');
                return;
            }
            else{
                res.render('index', {validacao:{}, dadosForm:{}});
                return;
            }
        });
}

module.exports = function(){
    return UsuariosDAO;
}