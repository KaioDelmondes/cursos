const http = require('http');

const reqData = {
    host : 'localhost',
    port : 80,
    path : '/teste', // alterar para '/' para obter o erro 404
    method : 'GET',
    headers : {
        'Accept' : 'application/json',
        'Content-Type' : 'application/json'
    }
}

const jsonData = {nome:'Kaio'};
const jsonDataString = JSON.stringify(jsonData);


 const request = http.request(reqData, function(res){
    var dataBuffer = [];
    res.on('data', function(chunk){
        dataBuffer.push(chunk);
    });
    
    res.on('end', function(){
        var data = Buffer.concat(dataBuffer).toString();
        console.log(data);
        console.log(res.statusCode);
    });
});

// request.write(jsonDataString);
request.end();