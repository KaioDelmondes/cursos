module.exports.formulario_inclusao_noticia = function (application, req, res) {
    res.render('admin/form_add_noticia', { validacao: {}, noticia: {} });
}

module.exports.salvar_noticia = function (application, req, res) {
    const dbConnection = application.config.dbConnection();
    const noticiaModel = new application.app.models.NoticiasDAO(dbConnection);

    req.assert('titulo', 'Título não pode ser vazio').notEmpty();
    req.assert('autor', 'Autor não pode ser vazio').notEmpty();
    req.assert('data_noticia', 'A data da notícia não pode ser vazio').notEmpty().isDate({ format: 'YYYY-MM-DD' });
    req.assert('resumo', 'Resumo não pode ser vazio').notEmpty();
    req.assert('resumo', 'O resumo deve ter entre 10 e 100 caracteres').len(10, 100);
    req.assert('noticia', 'Notícia não pode ser vazio').notEmpty();

    const validationErrors = req.validationErrors();

    if (validationErrors) {
        var noticia = req.body;
        res.render('admin/form_add_noticia', { validacao: validationErrors, noticia: noticia });
        return;
    }


    const noticiaData = Object.values(req.body);

    noticiaModel.salvarNoticia(noticiaData, (err, result) => {
        if (err) {
            res.send(noticiaData);
            console.log(err);
        } else {
            res.redirect('/noticias');
        }
    });
}