var mongodb = require('mongodb');
var express = require('express');
var bodyParser = require('body-parser');
var multiparty = require('connect-multiparty');
var fs = require('fs');

var app = express();
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(multiparty());

app.use(function(req, res, next){
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "content-type");
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.setHeader("Access-Control-Allow-Credentials", true);
    
    next();
});

const port = 8080;
app.listen(port);


var db = new mongodb.Db(
    'instagram',
    new mongodb.Server('localhost', 27017, {}),
    {}
);


console.log('Servidor rodando na porta ' + port);


app.get('/', function(req, res){
    res.send({msg:"Olá"});
});

//POST (insert)
app.post('/api', function(req, res){

    res.setHeader("Access-Control-Allow-Origin", "*");

    const urlName = Date.now() + '_' + req.files.arquivo.originalFilename;
    const actualPath = req.files.arquivo.path;
    const newPath = './uploads/' + urlName;

    fs.rename(actualPath, newPath, function(err){
        if(err){
            res.status(500).json(err);
            return;
        }
        var dados = {
            url_image : urlName,
            titulo : req.body.titulo
        };
        
        db.open(function(err, mongoClient){
            mongoClient.collection('postagens', function(err, collection){
                collection.insert(dados, function(err, records){
                    if(err){
                        res.status(500).json(err);
                    }
                    else{
                        res.status(200).send("Upload procedido com sucesso");
                    }
                    mongoClient.close();
                });
            });
        });
    });    
});

app.get('/api/imagens/:imagem', function(req, res){
    const urlImage = req.params.imagem;

    fs.readFile('./uploads/' + urlImage, function(err, data){
        if(err){
            res.status(400).send(err);
            return;
        }
        res.writeHead(200, {'Content-Type' : 'image/jpg'});
        res.status(200).end(data);
    });
});

//GET(find)
app.get('/api', function(req, res){
    res.setHeader("Access-Control-Allow-Origin", "*");

    db.open(function(err, mongoClient){
        mongoClient.collection('postagens', function(err, collection){
            collection.find().toArray(function(err, results){
                if(err){
                    res.json(err);
                }
                else{
                    res.json(results);
                }
                mongoClient.close();
            });
        });
    });
});

//GET by ID(find)
app.get('/api/:id', function(req, res){
    db.open(function(err, mongoClient){
        mongoClient.collection('postagens', function(err, collection){
            collection.find(mongodb.ObjectID(req.params.id)).toArray(function(err, results){
                if(err){
                    res.json(err);
                }
                else{
                    res.status(200).json(results);
                }
                mongoClient.close();
            });
        });
    });
});

//PUT by ID(update)
app.put('/api/:id', function(req, res){
    const postId = req.params.id, commentText = req.body.comment;

    db.open(function(err, mongoClient){
        mongoClient.collection('postagens', function(err, collection){
            collection.update(
                { _id : mongodb.ObjectID(postId) },
                { $push:
                    {
                        comentario:{
                            comentario_id : new mongodb.ObjectID(),
                            comentario_texto : commentText
                        }
                    }
                },
                {},
                function(err, records){
                    if(err){
                        res.status(500).json(err);
                    }
                    else{
                        res.status(200).json(records);
                    }
                    mongoClient.close();
                }
            );
        });
    });
});

//DELETE by ID(remove)
app.delete('/api/:id', function(req, res){
    const commentId = req.params.id;
    db.open(function(err, mongoClient){
        mongoClient.collection('postagens', function(err, collection){
            collection.update(
                {},
                {
                    $pull : {
                        comentario : {
                            comentario_id : mongodb.ObjectId(commentId)
                        }
                    }
                },
                {multi:true},
                function(err, records){
                    if(err){
                        res.status(500).json(err);
                    }
                    else{
                        res.status(200).json(records);
                    }
                    mongoClient.close();
                }
            );
        });
    });
});