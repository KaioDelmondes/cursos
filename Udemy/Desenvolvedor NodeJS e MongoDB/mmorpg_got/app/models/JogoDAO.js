const { ObjectId } = require('mongodb');

function JogoDAO(dbConnection){
    this._dbConnection = dbConnection();
}

JogoDAO.prototype.iniciarJogo = function(res, usuario, casa, msg){
    this._dbConnection.collection('jogo')
        .find(
            {
                usuario:usuario
            })
            .toArray(function(err, result){
                if(err){
                    console.log('-------ERRO NA PESQUISA DE JOGO: ' + err);
                }

                res.render('jogo', {casa: casa, jogo:result[0], msg: msg});
        });
}

JogoDAO.prototype.gerarValores = function(usuario){
    this._dbConnection.collection('jogo')
        .insertOne(
            {
                usuario:usuario,
                moeda:15,
                suditos: 10,
                temor: Math.floor(Math.random() * 1000),
                sabedoria: Math.floor(Math.random() * 1000),
                comercio: Math.floor(Math.random() * 1000),
                magia: Math.floor(Math.random() * 1000)
            },
            (err, result) => {
                if(err){
                    throw err;
                }
            }
        );
}

JogoDAO.prototype.getAcoes = function(usuario, res){
    const currentTime = Date.now();
    this._dbConnection.collection('acao')
        .find(
            {
                usuario:usuario,
                acao_termina_em:{$gt: currentTime}
            })
            .toArray(function(err, result){
                if(err){
                    console.log('-------ERRO NA PESQUISA DE JOGO: ' + err);
                }
                
                res.render('pergaminhos', {acoes: result});
        });
}

JogoDAO.prototype.novaAcao = function(acao){
    this._dbConnection.collection('acao')
        .insertOne(acao,
            (err, result) => {
                if(err){
                    throw err;
                }
            }
        );
}

JogoDAO.prototype.setMoedas = function(acaoInfo, res){
    this._dbConnection.collection('jogo')
        .updateOne(
            {usuario:acaoInfo.usuario},
            {$inc:{moeda: acaoInfo.moedas}},
            {multi: false},
            function(err, result){
                if (err) throw err;
                console.log(result);
                res.redirect('/jogo?msg=B');
            }
        );
}

JogoDAO.prototype.revogarOrdem = function(ordemId, res){
    this._dbConnection.collection('acao')
        .deleteOne(
            {_id: ObjectId(ordemId)},
            function(err, result){
                if (err) throw err;
                res.redirect('/jogo?msg=D');
            }
        );
}

module.exports = function(acao){
    return JogoDAO;
}

